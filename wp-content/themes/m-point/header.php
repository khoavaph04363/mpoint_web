<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/static/images/favicon.ico?v=2.0"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="l-nav">
    <div class="container clearfix">
        <?php if (ot_get_option('config_logo')) { ?>
            <div class="c-logo">
                <a href="<?= home_url(); ?>"><img alt="logo" src="<?= ot_get_option('config_logo'); ?>"></a>
            </div>
        <?php } ?>
        <div class="c-menu">
            <ul class="clearfix">
                <?php if (ot_get_option('config_header_signin')) { ?>
                    <li>
                        <a href="<?= ot_get_option('config_header_signin'); ?>" target="_blank"><i class="fas fa-user"></i>Đăng nhập</a>
                    </li>
                <?php } ?>
                <?php if (ot_get_option('config_header_register')) { ?>
                    <li>
                        <a href="<?= ot_get_option('config_header_register'); ?>" target="_blank"><i class="fas fa-cloud-download-alt"></i>Đăng ký</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div><!-- l-nav -->