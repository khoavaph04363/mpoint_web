//var window width
var viewportGlobal = $(window).width();

var calMatchHeight = function(){
  if($('.js-match-height').length > 0){
    $('.js-match-height >ul >li').matchHeight();
  }
};

var whySlider = function(){
  if($('.c-landing-grid ul').length > 0){
    $('.c-landing-grid ul').owlCarousel({
      loop:false,
      margin:60,
      responsiveClass:true,
      nav:true,
      dots:false,
      autoplay:false,
      autoHeight:false,
      autoplayTimeout:10000,
      autoplayHoverPause:false,
      navText:false,
      responsive:{
        0:{
          items:1
        },
        768:{
          items:2,
          margin:85
        },
        1200:{
          items:3,
          margin:86
        }
      }
    });
  }
};

var setMinHeight = function(minheight = 0) {
  $('.owl-carousel').each(function(i,e){
    var oldminheight = minheight;
    $(e).find('.owl-item li .c-landing-grid__outer').each(function(i,e){
      minheight = $(e).width() > minheight ? $(e).width() : minheight;
    });
    $(e).find('.owl-item li .c-landing-grid__outer').css("min-height",minheight + "px");
    $(e).find('.owl-item li .c-landing-grid__outer').css("min-width",minheight + "px");
    minheight = oldminheight;
  });
};

var showLogoList = function () {
  if($('.c-logo-more__btn').length > 0){
    $('.c-logo-more__btn').click(function(e) {
      e.preventDefault();
      var show = $('.c-logo-more__btn__show');
      var list = $('.c-logo-arcordian');
      if (show.hasClass('fa-caret-down')) {
        show.removeClass('fa-caret-down').addClass('fa-caret-up');
        list.removeClass('is-hide').addClass('is-show').slideDown();
      } else {
        show.removeClass('fa-caret-up').addClass('fa-caret-down');
        list.removeClass('is-show').addClass('is-hide').slideUp();
      }
    });
  }
};

var subLogoExpand = function(){
  $('.c-logo-arcordian-menu .c-logo-arcordian-btn').click(function(e) {
    e.preventDefault();
    var grand = $(this).parent().parent();
    var root = $(this).parent().parent().parent();
    if ($(this).hasClass('fa-angle-down')) {
      $(this).removeClass('fa-angle-down');
      $(this).addClass('fa-angle-up');
      $('li.mobile-active > .c-dropdown-arcordian',$(root)).slideUp();
      $('li.mobile-active > a > .c-logo-arcordian-btn',$(root)).removeClass('fa-angle-up');
      $('li.mobile-active > a > .c-logo-arcordian-btn',$(root)).addClass('fa-angle-down');
      $('li.mobile-active',$(root)).removeClass('mobile-active').addClass('mobile-hidden');
      $(grand).addClass('mobile-active').removeClass('mobile-hidden');
      $('>.c-dropdown-arcordian',$(grand)).slideDown();
    } else {
      $(this).removeClass('fa-angle-up');
      $(this).addClass('fa-angle-down');
      $(grand).removeClass('mobile-active').addClass('mobile-hidden');
      $('>.c-dropdown-arcordian',$(grand)).slideUp();
    }
  });
};

//function js footer content expand
var footerContentExpand = function(){
  if($('.c-footer-label').length > 0){
    $('.c-footer-label .fa').click(function() {
      var grand = $(this).parent().parent();
      if ($(this).hasClass('fa-plus')) {
        $(this).removeClass('fa-plus');
        $(this).addClass('fa-minus');
        $('.c-footer-content',$(grand)).slideDown();
      } else {
        $(this).removeClass('fa-minus');
        $(this).addClass('fa-plus');
        $('.c-footer-content',$(grand)).slideUp();
      }
    });
  }
};

//function go top
var goTop = function(){
  //hide button top on top
  // var viewport = $(window).width();
  // if (viewport > 991){
  $(window).scroll(function () {
    if ($(this).scrollTop() > 150) {
      $('.c-gotop').fadeIn();
    } else {
      $('.c-gotop').fadeOut();
    }
  });
  //}
  // button top
  $('.c-gotop').click(function(e){
    e.preventDefault();
    $("html, body").animate({scrollTop: $('body').offset().top}, 700);
  });
};

//function go point
var goPoint = function(){
  // button go to contact block
  bonus = 60;
  if ($('body').hasClass('customize-support')){
    bonus = bonus + $('#wpadminbar').height();
  }
  $('.js-go-point').click(function(e){
    e.preventDefault();
    var id = $(this).attr('href');
    $('html, body').animate({scrollTop: $(id).offset().top - bonus}, 700);
  });
};

var aosAnimate = function () {
  AOS.init();
};