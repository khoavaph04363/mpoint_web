<?php

function wpbase_pagination($pages = '', $range = 2)
{
  $showitems = ($range * 2) + 1;
  global $paged;
  if(empty($paged)) $paged = 1;
  if($pages == '')
  {
    global $wp_query;
    $pages = $wp_query->max_num_pages;

    if(!$pages)
      $pages = 1;
  }

  if(1 != $pages)
  {
    //echo '<div class="b-page">';
    echo '<ul class="pagination">';

    echo '<li class="page-item disabled hidden-md-down d-none d-lg-block"><span class="page-link">'.__('Page', 'efora').' '.$paged.' '.__('of', 'efora').' '.$pages.'</span></li>';

    if($paged > 2 && $paged > $range+1 && $showitems < $pages)
      echo '<li class="page-item"><a class="page-link" href="'.get_pagenum_link(1).'" aria-label="First Page">&laquo;<span class="hidden-sm-down d-none d-md-inline"> '.__('First', 'efora').'</span></a></li>';

    if($paged > 1 && $showitems < $pages)
      echo '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($paged - 1).'" aria-label="Previous Page">&lsaquo;<span class="hidden-sm-down d-none d-md-inline"> '.__('Prev', 'efora').'</span></a></li>';

    for ($i=1; $i <= $pages; $i++)
    {
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
        echo ($paged == $i)? '<li class="page-item active"><span class="page-link"><span class="sr-only">'.__('Current Page', 'efora').' </span>'.$i.'</span></li>' : '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($i).'"><span class="sr-only">'.__('Page', 'efora').' </span>'.$i.'</a></li>';
    }

    if ($paged < $pages && $showitems < $pages)
      echo '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($paged + 1).'" aria-label="Next Page"><span class="hidden-sm-down d-none d-md-inline">'.__('Next', 'efora').' </span>&rsaquo;</a></li>';

    if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages)
      echo '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($pages).'" aria-label="Last Page"><span class="hidden-sm-down d-none d-md-inline">'.__('Last', 'efora').' </span>&raquo;</a></li>';

    echo '</ul>';
    //echo '</div>';
    //echo '<div class="pagination-info mb-5 text-center">[ <span class="text-muted">Page</span> '.$paged.' <span class="text-muted">of</span> '.$pages.' ]</div>';
  }
}

?>