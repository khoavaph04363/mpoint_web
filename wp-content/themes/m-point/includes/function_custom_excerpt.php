<?php

// Custom Excerpts
function wpbase_index($length) // Create 20 Word Callback for Index page Excerpts, call using wpbase_excerpt('wpbase_index');
{
  return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using wpbase_excerpt('wpbase_custom_post');
function wpbase_custom_post($length)
{
  return 40;
}

// Create the Custom Excerpts callback
function wpbase_excerpt($length_callback = '', $more_callback = '')
{
  global $post;
  if (function_exists($length_callback)) {
    add_filter('excerpt_length', $length_callback);
  }
  if (function_exists($more_callback)) {
    add_filter('excerpt_more', $more_callback);
  }
  $output = get_the_excerpt();
  $output = apply_filters('wptexturize', $output);
  $output = apply_filters('convert_chars', $output);
  $output = '<p>' . $output . '</p>';
  echo $output;
}

// Create the Custom Excerpts callback no p
function wpbase_excerpt_clear($length_callback = '', $more_callback = '')
{
  global $post;
  if (function_exists($length_callback)) {
    add_filter('excerpt_length', $length_callback);
  }
  if (function_exists($more_callback)) {
    add_filter('excerpt_more', $more_callback);
  }
  $output = get_the_excerpt();
  $output = apply_filters('wptexturize', $output);
  $output = apply_filters('convert_chars', $output);
  $output = '' . $output . '';
  echo $output;
}

?>