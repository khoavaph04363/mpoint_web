<?php

function register_menus()
{
  register_nav_menus(
    array(
        'footer-col1-menu' => __('Footer Col 1 Menu', 'mpoint'),

    )
  );
}

add_action('init', 'register_menus');

function wpbase_wp_nav_menu_args($args = '')
{
  $args['container'] = false;
  return $args;
}

add_filter('wp_nav_menu_args', 'wpbase_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation

function wpbase_footer_col1_nav()
{
  wp_nav_menu(
      array(
          'theme_location'  => 'footer-col1-menu',
          'menu'            => '',
          'container'       => 'div',
          'container_class' => 'menu-{menu slug}-container',
          'container_id'    => '',
          'menu_class'      => 'c-footer-content',
          'menu_id'         => '',
          'echo'            => true,
          'fallback_cb'     => 'wp_page_menu',
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<ul class="c-footer-list">%3$s</ul>',
          'depth'           => 0,
          'walker'          => ''
      )
  );
}