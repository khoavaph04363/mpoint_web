<?php
// In your functions.php
class Nav_Main_Walker extends Walker_Nav_Menu {

  function start_lvl(&$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output = preg_replace( "/(.*)(\<li.*?class\=\")([^\"]*)(\".*?)$/", "$1$2$3 c-menu-child$4", $output );
    $output .= "\n$indent<div class=\"c-dropdown-menu\"><ul class=\"sub-menu\">\n";
  }

  function end_lvl( &$output, $depth = 0,$args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "</ul></div>$indent\n";
  }

  function start_el ( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
    global $wp_query, $wpdb;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $li_attributes = '';
    $class_names = $value = '';
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    //Add class and attribute to LI element that contains a submenu UL.
//    if ($args->has_children){
//      $classes[]    = 'dropdown';
//      $li_attributes .= 'data-dropdown="dropdown"';
//    }
    $classes[] = 'menu-item-' . $item->ID;
    //If we are on the current page, add the active class to that menu item.
    $classes[] = ($item->current) ? 'active' : '';
    //Make sure you still add all of the WordPress classes.
    $class_names = join( ' ', apply_filters( 'nav_menu_css_class',     array_filter( $classes ), $item, $args ) );
    $class_names = ' class="' . esc_attr( $class_names ) . '"';
    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
    $has_children = $wpdb->get_var(
        $wpdb->prepare("
       SELECT COUNT(*) FROM $wpdb->postmeta
       WHERE meta_key = %s
       AND meta_value = %d
       ", '_menu_item_menu_item_parent', $item->ID)
    );
    //$output .= $indent . '<li' . $id . $value . $class_names .'>';
    $output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';
    //Add attributes to link element.
    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn ) ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url ) ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
    // Check if menu item is in main menu

//    if ( $depth == 0 && $has_children > 0  ) {
//      // These lines adds your custom class and attribute
//      $attributes .= ' class="dropdown-toggle"';
//      $attributes .= ' data-toggle="dropdown"';
//    }
    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    // Add the caret if menu level is 0
    if ( $depth == 0 && $has_children > 0  ) {
      $item_output .= ' <span class="c-submenu-btn fa fa-angle-down"></span>';
    }
    $item_output .= '</a>';
    $item_output .= $args->after;
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
}
