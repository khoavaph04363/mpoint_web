<?php /* Template Name: Page Custom */ ?>
<?php get_header(); ?>

<?php
$bannerimgdefault = ot_get_option('banner_bg');
$bannerimg = get_field('banner_bg');
$size = 'full';
$bannerimgurl = wp_get_attachment_image_src($bannerimg, $size);
?>
<?php if($bannerimg){ ?>
	<div class="l-banner__detail" style="background-image: url('<?php echo $bannerimgurl[0]; ?>');">
	<?php } else { ?>
		<div class="l-banner__detail" style="background-image: url('<?php echo $bannerimgdefault; ?>');">
		<?php } ?>
		<div class="c-caption">
			<div class="container">
				<div class="b-table">
					<div class="b-table__row">
						<div class="b-table__title">
							<?php the_title(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="c-block-2 is-breadcrumb">
		<div class="container">
			<div class="c-yoast-breadcrumb">
				<?php if (function_exists('yoast_breadcrumb')) {
					yoast_breadcrumb();
				} ?>
			</div>
		</div>
	</div>
	<div class="l-content">
		<div class="container">
			<div class="c-block-3">
				<div class="c-box">
					<div class="b-maincontent">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					</div>
				</div><!-- c-box -->
			</div><!-- c-block -->
		</div><!-- container -->
	</div><!-- l-content -->

	<?php get_footer(); ?>