<div class="b-grid">
  <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
    <div class="b-grid__img">
      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php the_post_thumbnail('medium'); // Declare pixel size you need inside the array ?>
        <?php
        $category = get_the_category();
        $category_display = $category[0]->name;
        ?>
        <p><span><?php echo $category_display; ?></span></p>
      </a>
    </div>
  <?php endif; ?>
  <div class="b-grid__content">
    <div class="b-grid__row"><a class="b-grid__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
    <div class="b-grid__row b-grid__desc">
      <i class="fa fa-clock-o" aria-hidden="true"></i><span><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' trước'; ?></span>
    </div>
  </div>
</div>