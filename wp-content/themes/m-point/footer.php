<div class="l-footer">
    <div class="c-footer-main">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <?php if (ot_get_option('config_logo')) { ?>
                        <div class="c-footer-logo"><a href="<?php echo home_url(); ?>"><img alt="logo"
                                                                                            src="<?= ot_get_option('config_logo') ?>"/></a>
                        </div>
                    <?php } ?>
                    <?php if (ot_get_option('config_text_footer') || ot_get_option('config_link_cooperate')) { ?>
                        <div class="c-footer-text">
                            <?php if (ot_get_option('config_text_footer')) { ?>
                                <div class="b-maincontent">
                                    <?= ot_get_option('config_text_footer') ?>
                                </div>
                            <?php } ?>
                            <?php if (ot_get_option('config_link_cooperate')) { ?>
                                <div class="c-footer-button">
                                    <a href="<?= ot_get_option('config_link_cooperate') ?>">Hợp tác</a>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div><!-- col -->
                <div class="col-md-6 col-lg-3">
                    <div class="c-footer-label is-top">Giới thiệu<i class="fa fa-plus"></i></div>
                    <div class="c-footer-content">
                        <?php wpbase_footer_col1_nav(); ?>
                    </div><!-- c-footer-content -->
                </div><!-- col -->
                <?php if (ot_get_option('config_text_hanoi') || ot_get_option('config_text_hcm')) { ?>
                    <div class="col-md-6 col-lg-3">
                        <div class="c-footer-label is-top d-md-none">Dịch vụ khách hàng<i class="fa fa-plus"></i></div>
                        <div class="c-footer-content">
                            <?php if (ot_get_option('config_text_hanoi')) { ?>
                                <div class="c-footer-label is-top-2">Chi nhánh Hà Nội</div>
                                <div class="c-footer-text">
                                    <div class="b-maincontent">
                                        <?= ot_get_option('config_text_hanoi'); ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if (ot_get_option('config_text_hcm')) { ?>
                                <div class="c-footer-label">Chi nhánh TP. Hồ Chí Minh</div>
                                <div class="c-footer-text">
                                    <div class="b-maincontent">
                                        <?= ot_get_option('config_text_hcm'); ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div><!-- c-footer-content -->
                    </div><!-- col -->
                <?php } ?>
                <?php if (ot_get_option('config_text_website') || ot_get_option('config_email') || ot_get_option('config_phone')) { ?>
                    <div class="col-md-6 col-lg-3">
                        <div class="c-footer-label is-top">Thông tin liên hệ<i class="fa fa-plus"></i></div>
                        <div class="c-footer-content">
                            <div class="c-footer-text">
                                <div class="b-maincontent">
                                    <p>Liên hệ với chúng tôi khi bạn có yêu cầu. </p>
                                </div>
                            </div>
                            <div class="c-footer-icon">
                                <ul>
                                    <?php if (ot_get_option('config_text_website')) { ?>
                                        <li class="clearfix">
                                            <div class="c-footer-icon__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/static/images/mpoint/icon-home.png">
                                            </div>
                                            <div class="c-footer-icon__content">
                                                <p>Website:</p>
                                                <a href="<?= ot_get_option('config_text_website'); ?>"
                                                   target="_blank"><?= ot_get_option('config_text_website'); ?></a>
                                            </div>
                                        </li>
                                    <?php } ?>
                                    <?php if (ot_get_option('config_email')) { ?>
                                        <li class="clearfix">
                                            <div class="c-footer-icon__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/static/images/mpoint/icon-mail.png">
                                            </div>
                                            <div class="c-footer-icon__content">
                                                <p>Email:</p>
                                                <a href="mailto:<?= ot_get_option('config_email'); ?>"><?= ot_get_option('config_email'); ?></a>
                                            </div>
                                        </li>
                                    <?php } ?>
                                    <?php if (ot_get_option('config_phone')) { ?>
                                        <li class="clearfix">
                                            <div class="c-footer-icon__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/static/images/mpoint/icon-phone.png">
                                            </div>
                                            <div class="c-footer-icon__content">
                                                <p>Phone:</p>
                                                <a href="tel:<?= ot_get_option('config_phone'); ?>"><?= ot_get_option('config_phone'); ?></a>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div><!-- c-footer-content -->
                    </div><!-- col -->
                <?php } ?>
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- c-footer-main -->
    <div class="c-footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <ul class="clearfix">
                        <li><a class="js-go-point" href="#smart-feature">Tính năng thông minh</a></li>
                        <li><a class="js-go-point" href="#why">Tại sao chọn chúng tôi</a></li>
                        <li><a class="js-go-point" href="#download">Tải về ngay</a></li>
                        <li><a class="js-go-point" href="#partner">Khách hàng & Đối tác</a></li>
                    </ul>
                </div>
                <?php if (ot_get_option('config_app_store') || ot_get_option('config_link_google_play')) { ?>
                    <div class="col-md-4">
                        <div class="c-footer-app">
                            <span>Get the mPoint App</span>
                            <div class="c-footer-app__download">
                                <?php if (ot_get_option('config_app_store')) { ?>
                                    <a href="<?= ot_get_option('config_app_store'); ?>" target="_blank">
                                        <img alt="app store"
                                             src="<?php echo get_template_directory_uri(); ?>/static/upload/app-store.png">
                                    </a>
                                <?php } ?>
                                <?php if (ot_get_option('config_link_google_play')) { ?>
                                    <a href="<?= ot_get_option('config_link_google_play'); ?>" target="_blank">
                                        <img alt="google play"
                                             src="<?php echo get_template_directory_uri(); ?>/static/upload/google-play.png">
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if (ot_get_option('config_copyright')) { ?>
                    <div class="col-md-4">
                        <div class="c-copyright">
                            <?= ot_get_option('config_copyright'); ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div><!-- l-footer -->
<div class="c-gotop">
    <i class="to-top"></i>
</div>
<injectjs/>
<?php wp_footer(); ?>
</body>
</html>