<div class="c-landing-footer clearfix" style="background-image: url('<?= wp_get_attachment_image_url($background, [1920, 0]) ?>')">
    <div class="c-landing-footer__right" data-aos="fade-left" data-aos-duration="1500">
        <div class="c-landing-footer__circle">
            <?php if (!empty($title)): ?>
                <div class="c-landing-footer__circle__title">
                    <?= $title; ?>
                </div>
            <?php endif; ?>
            <?php if (ot_get_option('config_link_n') || ot_get_option('config_qr_code')) { ?>
                <div class="c-landing-footer__circle__logo">
                    <?php if (ot_get_option('config_qr_code')) { ?>
                        <a href="#">
                            <img alt="download" src="<?= ot_get_option('config_qr_code'); ?>">
                        </a>
                    <?php } ?>
                    <?php if (ot_get_option('config_link_n')) { ?>
                        <a href="<?= ot_get_option('config_link_n') ?>" target="_blank">
                            <img alt="download"
                                 src="<?php echo get_template_directory_uri(); ?>/static/upload/logo-white-4.png">
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php if (!empty($image)): ?>
        <div class="c-landing-footer__left" data-aos="fade-right" data-aos-duration="1500">
            <img src="<?= wp_get_attachment_image_url($image, [1920, 0]) ?>" alt="phone">
        </div>
    <?php endif; ?>
</div><!-- c-landing-footer -->