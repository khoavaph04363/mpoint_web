<?php

class SOW_Title_Img extends SiteOrigin_Widget {

  function __construct() {

    parent::__construct(
        'sow-title-img', __('Block Ảnh và Tính năng', 'mpoint'), array(
        'description' => __('Block hiển thị ảnh bên trái và link nền xanh tròn', 'mpoint'),
    ), array(), false, plugin_dir_path(__FILE__)
    );
  }

  /**
   * Initialize the image grid, mainly to add scripts and styles.
   */
  function initialize() {

  }

  function get_widget_form() {

    return array(
        'background' => array(
            'type' => 'media',
            'label' => __('Ảnh nền', 'mpoint')
        ),
        'title' => array(
            'type' => 'text',
            'label' => __('Tiêu đề', 'mpoint'),
        ),
        'image' => array(
            'type' => 'media',
            'label' => __('Ảnh bên trái', 'mpoint')
        ),
    );
  }

  function get_template_variables( $instance, $args ) {
    $background = isset( $instance['background'] ) ? $instance['background'] : '';
    $title = isset( $instance['title'] ) ? $instance['title'] : '';
    $image = isset( $instance['image'] ) ? $instance['image'] : '';

    return array(
        'background' => $background,
        'title' => $title,
        'image' => $image,
    );
  }

}

siteorigin_widget_register('sow-title-img', __FILE__, 'SOW_Title_Img');
