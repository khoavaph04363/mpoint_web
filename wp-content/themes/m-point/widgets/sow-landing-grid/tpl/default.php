<div class="c-block" style="background-image: url('<?= wp_get_attachment_image_url($background, [1920, 0]) ?>')">
    <div class="c-box">
        <?php if (!empty($contents)): ?>
            <div class="c-landing-two">
                <?php foreach ($contents as $content) : ?>
                    <?php
                    $blue = '';
                    if ($content['is_blue'] == true) {
                        $blue = ' is-blue';
                    }
                    $fade = 'fade-left';
                    if ($content['is_reverse'] == true) {
                        $fade = 'fade-right';
                    }
                    ?>
                    <div class="c-landing-two__item clearfix<?= $blue; ?>">
                        <div class="container">
                            <?php if (!empty($content['images'])): ?>
                                <div class="c-landing-two__img" data-aos="<?= $fade; ?>" data-aos-duration="1500">
                                    <img alt="information"
                                         src="<?= wp_get_attachment_image_url($content['images'], [1920, 0]) ?>">
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($content['title']) || !empty($content['description'])): ?>
                                <div class="c-landing-two__content">
                                    <?php if (!empty($content['title'])): ?>
                                        <div class="c-landing-two__content__title">
                                            <?= $content['title']; ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($content['description'])): ?>
                                        <div class="b-maincontent">
                                            <?= $content['description']; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div><!-- c-block -->