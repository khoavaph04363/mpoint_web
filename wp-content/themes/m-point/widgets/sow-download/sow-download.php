<?php

class SOW_Download extends SiteOrigin_Widget {

  function __construct() {

    parent::__construct(
        'sow-download', __('Block Tải về', 'mpoint'), array(
        'description' => __('Block hiển thị nội dung tải về + ảnh', 'mpoint'),
    ), array(), false, plugin_dir_path(__FILE__)
    );
  }

  /**
   * Initialize the image grid, mainly to add scripts and styles.
   */
  function initialize() {

  }

  function get_widget_form() {

    return array(
        'background' => array(
            'type' => 'media',
            'label' => __('Ảnh nền', 'mpoint')
        ),
        'title' => array(
            'type' => 'text',
            'label' => __('Tiêu đề', 'mpoint'),
        ),
        'content' => array(
            'type' => 'tinymce',
            'label' => __('Nội dung', 'mpoint'),
            'rows' => 10
        ),
        'image' => array(
            'type' => 'media',
            'label' => __('Ảnh bên trái', 'mpoint')
        ),
    );
  }

  function get_template_variables( $instance, $args ) {
    $background = isset( $instance['background'] ) ? $instance['background'] : '';
    $title = isset( $instance['title'] ) ? $instance['title'] : '';
    $content = isset( $instance['content'] ) ? $instance['content'] : '';
    $image = isset( $instance['image'] ) ? $instance['image'] : '';

    return array(
        'background' => $background,
        'title' => $title,
        'content' => $content,
        'image' => $image,
    );
  }

}

siteorigin_widget_register('sow-download', __FILE__, 'SOW_Download');
