<div class="c-block clearfix" id="download" style="background-image: url('<?= wp_get_attachment_image_url($background, [1920, 0]) ?>')">
    <div class="c-landing-download clearfix">
        <div class="c-landing-download__left">
            <?php if (!empty($title)): ?>
                <div class="c-landing-download__title">
                    <?= $title; ?>
                </div>
            <?php endif; ?>
            <?php if (!empty($content)): ?>
                <div class="c-landing-download__des">
                    <div class="b-maincontent">
                        <?= $content; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (ot_get_option('config_app_store') || ot_get_option('config_link_google_play') || ot_get_option('config_qr_code')) { ?>
                <div class="c-landing-download__logo">
                    <?php if (ot_get_option('config_app_store')) { ?>
                        <a href="<?= ot_get_option('config_app_store'); ?>" target="_blank">
                            <img alt="download"
                                 src="<?php echo get_template_directory_uri(); ?>/static/upload/logo-white-1.png">
                        </a>
                    <?php } ?>
                    <?php if (ot_get_option('config_link_google_play')) { ?>
                        <a href="<?= ot_get_option('config_link_google_play'); ?>" target="_blank">
                            <img alt="download"
                                 src="<?php echo get_template_directory_uri(); ?>/static/upload/logo-white-2.png">
                        </a>
                    <?php } ?>
                    <?php if (ot_get_option('config_qr_code')) { ?>
                        <a href="#">
                            <img alt="download" src="<?= ot_get_option('config_qr_code'); ?>">
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <?php if (!empty($image)): ?>
            <div class="c-landing-download__right" data-aos="fade-left" data-aos-duration="1500">
                <img src="<?= wp_get_attachment_image_url($image, [1920, 0]) ?>" alt="download">
            </div>
        <?php endif; ?>
    </div>
</div><!-- c-block -->