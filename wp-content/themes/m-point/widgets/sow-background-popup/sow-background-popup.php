<?php

class SOW_Background_Popup extends SiteOrigin_Widget {

  function __construct() {

    parent::__construct(
        'sow-background-popup', __('Block ảnh nền và Popup', 'mpoint'), array(
        'description' => __('Block Hiển thị ảnh nền và popup', 'mpoint'),
    ), array(), false, plugin_dir_path(__FILE__)
    );
  }

  /**
   * Initialize the image grid, mainly to add scripts and styles.
   */
  function initialize() {

  }

  function get_widget_form() {

    return array(
        'title' => array(
            'type' => 'text',
            'label' => __('Tiêu đề', 'mpoint'),
        ),
        'description' => array(
            'type' => 'text',
            'label' => __('Tiêu đề bên dưới', 'mpoint'),
        ),
        'background' => array(
            'type' => 'media',
            'label' => __('Ảnh nền', 'mpoint')
        ),
        'title_popup' => array(
            'type' => 'text',
            'label' => __('Tiêu đề Popup', 'mpoint'),
        ),
        'description_popup' => array(
            'type' => 'tinymce',
            'label' => __('Nội dung popup', 'mpoint'),
            'rows' => 5
        ),
    );
  }

  function get_template_variables( $instance, $args ) {
    $title = isset( $instance['title'] ) ? $instance['title'] : '';
    $description = isset( $instance['description'] ) ? $instance['description'] : '';
    $background = isset( $instance['background'] ) ? $instance['background'] : '';
    $title_popup = isset( $instance['title_popup'] ) ? $instance['title_popup'] : '';
    $description_popup = isset( $instance['description_popup'] ) ? $instance['description_popup'] : '';

    return array(
        'title' => $title,
        'description' => $description,
        'background' => $background,
        'title_popup' => $title_popup,
        'description_popup' => $description_popup,
    );
  }

}

siteorigin_widget_register('sow-background-popup', __FILE__, 'SOW_Background_Popup');
