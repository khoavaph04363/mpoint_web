<div class="c-popup-bg" class="btn btn-primary" data-target="#popupmodal" data-toggle="modal"
     style="background-image: url('<?= wp_get_attachment_image_url($background, [1920, 0]) ?>')">
    <div class="container">
        <?php if (!empty($title) || !empty($description)): ?>
            <div class="c-popup-bg__title">
                <?php if (!empty($title)): ?>
                    <span class="top"><?= $title ?></span>
                <?php endif; ?>
                <?php if (!empty($description)): ?>
                    <span class="bottom"><?= $description ?></span>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div><!-- c-popup-bg -->
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade c-popup" id="popupmodal" role="dialog"
     tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?php if (!empty($title_popup)): ?>
                    <h5 class="modal-title" id="exampleModalLabel"><?= $title_popup ?></h5>
                <?php endif; ?>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true"><i class="close-btn"></i></span>
                </button>
            </div>
            <?php if (!empty($description_popup)): ?>
                <div class="modal-body">
                    <div class="b-maincontent">
                        <?= $description_popup ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>