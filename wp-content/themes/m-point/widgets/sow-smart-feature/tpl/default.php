<div class="c-block" style="background-image: url('<?= wp_get_attachment_image_url($background, [1920, 0]) ?>')" id="smart-feature">
    <div class="container">
        <div class="c-box">
            <div class="c-landing-smart">
                <div class="row">
                    <div class="col-md-3" data-aos="fade-right" data-aos-duration="1500">
                        <?php if (!empty($image)): ?>
                            <img src="<?= wp_get_attachment_image_url($image, [1920, 0]) ?>">
                        <?php endif; ?>
                    </div>
                    <div class="col-md-9">
                        <?php if (!empty($title)): ?>
                            <div class="c-box__title is-white"><?= $title; ?></div>
                        <?php endif; ?>
                        <?php if (!empty($contents)): ?>
                            <div class="c-landing-smart__list">
                                <ul class="clearfix">
                                    <?php foreach ($contents as $content) : ?>
                                        <li>
                                            <?php if (!empty($content['title'])): ?>
                                                <p><?= $content['title'] ?></p>
                                            <?php endif; ?>
                                            <?php if (!empty($content['description'])): ?>
                                                <span><?= $content['description'] ?></span>
                                            <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- c-block -->