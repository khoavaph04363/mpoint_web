<?php

class SOW_Smart_Feature extends SiteOrigin_Widget {

	function __construct() {

		parent::__construct(
			'sow-smart-feature', __('Block Tính năng thông minh', 'mpoint'), array(
				'description' => __('Block hiển thị tính năng thông minh', 'mpoint'),
			), array(), false, plugin_dir_path(__FILE__)
		);
	}

  /**
	* Initialize the image grid, mainly to add scripts and styles.
	*/
	function initialize() {

	}

	function get_widget_form() {

		return array(
			'title' => array(
				'type' => 'text',
				'label' => __('Tiêu đề', 'mpoint'),
			),
            'background' => array(
                'type' => 'media',
                'label' => __('Ảnh nền', 'mpoint')
            ),
            'image' => array(
                'type' => 'media',
                'label' => __('Ảnh bên trái', 'mpoint')
            ),
            'contents' => array(
                'type' => 'repeater',
                'label' => __('Nội dung', 'mpoint'),
                'item_name'  => __( 'Nội dung', 'mpoint' ),
                'item_label' => array(
                    'selector'     => "[name*='title']",
                    'update_event' => 'change',
                    'value_method' => 'val'
                ),
                'fields' => array(
                    'title' => array(
                        'type' => 'text',
                        'label' => __('Tiêu đề', 'mpoint')
                    ),
                    'description' => array(
                        'type' => 'text',
                        'label' => __('Nội dung', 'mpoint')
                    ),
                )
            ),
		);
	}

	function get_template_variables( $instance, $args ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
        $background = isset( $instance['background'] ) ? $instance['background'] : '';
        $image = isset( $instance['image'] ) ? $instance['image'] : '';
        $contents = isset( $instance['contents'] ) ? $instance['contents'] : array();

		return array(
			'title' => $title,
            'background' => $background,
            'image' => $image,
            'contents' => $contents,
		);
	}

}

siteorigin_widget_register('sow-smart-feature', __FILE__, 'SOW_Smart_Feature');
