<div class="c-banner">
    <div class="c-banner__item"
         style="background-image: url('<?= wp_get_attachment_image_url($background, [1920, 0]) ?>')">
        <div class="c-banner__box">
            <div class="container">
                <div class="c-banner__outer" data-aos="fade-right" data-aos-duration="1500">
                    <?php if (!empty($content)): ?>
                        <div class="b-maincontent">
                            <?= $content; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (ot_get_option('config_qr_code')) { ?>
                        <div class="c-banner__qr">
                            <h3>Download <br/>app tại:</h3>
                            <img alt="qrcode" src="<?= ot_get_option('config_qr_code'); ?>">
                        </div>
                    <?php } ?>
                    <?php if (ot_get_option('config_app_store') || ot_get_option('config_link_google_play')) { ?>
                        <div class="c-banner__download clearfix">
                            <?php if (ot_get_option('config_app_store')) { ?>
                                <a href="<?= ot_get_option('config_app_store'); ?>" target="_blank">
                                    <img alt="app store"
                                         src="<?php echo get_template_directory_uri(); ?>/static/upload/app-store.png">
                                </a>
                            <?php } ?>
                            <?php if (ot_get_option('config_link_google_play')) { ?>
                                <a href="<?= ot_get_option('config_link_google_play'); ?>" target="_blank">
                                    <img alt="google play"
                                         src="<?php echo get_template_directory_uri(); ?>/static/upload/google-play.png">
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php if (!empty($image)): ?>
            <div class="c-banner__img" data-aos="fade-left" data-aos-duration="1500">
                <div class="container">
                    <img alt="phone" src="<?= wp_get_attachment_image_url($image, [1920, 0]) ?>">
                </div>
            </div>
        <?php endif ?>
    </div>
</div><!-- c-banner -->