<?php

class SOW_Banner extends SiteOrigin_Widget {

  function __construct() {

    parent::__construct(
        'sow-banner', __('Block Banner', 'mpoint'), array(
        'description' => __('Block hiển thị banner ở trang chủ', 'mpoint'),
    ), array(), false, plugin_dir_path(__FILE__)
    );
  }

  /**
   * Initialize the image grid, mainly to add scripts and styles.
   */
  function initialize() {

  }

  function get_widget_form() {

    return array(
        'background' => array(
            'type' => 'media',
            'label' => __('Ảnh nền', 'mpoint')
        ),
        'content' => array(
            'type' => 'tinymce',
            'label' => __('Nội dung', 'mpoint'),
            'rows' => 5
        ),
        'image' => array(
            'type' => 'media',
            'label' => __('Ảnh bên phải', 'mpoint')
        ),
    );
  }

  function get_template_variables( $instance, $args ) {
    $background = isset( $instance['background'] ) ? $instance['background'] : '';
    $content = isset( $instance['content'] ) ? $instance['content'] : '';
    $image = isset( $instance['image'] ) ? $instance['image'] : '';

    return array(
        'background' => $background,
        'content' => $content,
        'image' => $image,
    );
  }

}

siteorigin_widget_register('sow-banner', __FILE__, 'SOW_Banner');
