<div class="c-block" id="partner">
    <div class="container">
        <div class="c-box">
            <?php if (!empty($title)): ?>
                <div class="c-box__title">
                    <?= $title; ?>
                </div>
            <?php endif; ?>
            <?php if (!empty($content)): ?>
                <div class="c-box__des">
                    <div class="b-maincontent">
                        <?= $content; ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="c-logo-list" data-aos="fade-down" data-aos-duration="1500">
                <ul class="clearfix">
                    <?php
                    global $post;
                    $args_partner = array(
                        'post_type' => 'partner',
                        'post_status' => 'publish',
                        'posts_per_page' => 12,
                        'order' => 'DESC',
                        'orderby' => 'meta_value_num',
                        'meta_key' => 'partner_order',
                    );
                    $myposts = get_posts($args_partner);
                    foreach ($myposts as $post) :
                        setup_postdata($post);
                        $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false, '');
                        ?>
                        <li>
                            <div class="c-logo-list-item">
                                <div class="c-logo-list-item__inner"><a href="#"><img
                                                alt="<?php the_title(); ?>" src="<?php echo $src[0]; ?>"/></a></div>
                            </div>
                        </li>
                    <?php endforeach;
                    wp_reset_postdata(); ?>
                </ul>
            </div>
            <?php
            $count_posts = wp_count_posts('partner')->publish;
            if ($count_posts >= 12) {
                ?>
                <div class="c-logo-more" data-aos="fade-down" data-aos-duration="1500">
                    <button class="c-logo-more__btn">
                        xem thêm<i class="fas c-logo-more__btn__show fa-caret-down"></i>
                    </button>
                    <div class="c-logo-arcordian">
                        <div class="c-logo-arcordian-menu">
                            <ul><?php
                                $partner_terms_content = get_terms(
                                    array(
                                        'taxonomy' => 'partner_category',
                                        'hide_empty' => true,
                                    )
                                );
                                if (!empty($partner_terms_content) && is_array($partner_terms_content)) {
                                    foreach ($partner_terms_content as $partner_term_content) {
                                        ?>
                                        <li>
                                            <a href="#"><?php echo $partner_term_content->name ?><span
                                                        class="c-logo-arcordian-btn fa fa-angle-down"></span></a>
                                            <div class="c-dropdown-arcordian c-logo-list">
                                                <ul class="clearfix">
                                                    <?php
                                                    $partner_term_content_slug = $partner_term_content->slug;
                                                    $_posts = new WP_Query(array(
                                                        'post_type' => 'partner',
                                                        'posts_per_page' => -1,
                                                        'tax_query' => array(
                                                            array(
                                                                'taxonomy' => 'partner_category',
                                                                'field' => 'slug',
                                                                'terms' => $partner_term_content_slug,
                                                            ),
                                                        ),
                                                    ));
                                                    if ($_posts->have_posts()) : while ($_posts->have_posts()) : $_posts->the_post();
                                                        ?>
                                                        <li>
                                                            <div class="c-logo-list-item">
                                                                <div class="c-logo-list-item__inner"><a href="#"><img
                                                                                alt="<?php the_title(); ?>"
                                                                                src="<?php the_post_thumbnail_url(); ?>"/></a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php endwhile; ?>
                                                    <?php
                                                    endif;
                                                    wp_reset_postdata();
                                                    ?>
                                                </ul>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div><!-- c-menu -->
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div><!-- c-block -->