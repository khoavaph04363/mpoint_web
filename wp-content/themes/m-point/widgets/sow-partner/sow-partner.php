<?php

class SOW_Partner extends SiteOrigin_Widget {

  function __construct() {

    parent::__construct(
        'sow-partner', __('Block Khách hàng & Đối tác', 'mpoint'), array(
        'description' => __('Block hiển thị logo khách hàng & đối tác', 'mpoint'),
    ), array(), false, plugin_dir_path(__FILE__)
    );
  }

  /**
   * Initialize the image grid, mainly to add scripts and styles.
   */
  function initialize() {

  }

  function get_widget_form() {

    return array(
        'title' => array(
            'type' => 'text',
            'label' => __('Tiêu đề', 'mpoint'),
        ),
        'content' => array(
            'type' => 'tinymce',
            'label' => __('Nội dung', 'mpoint'),
            'rows' => 10
        ),
    );
  }

  function get_template_variables( $instance, $args ) {
    $title = isset( $instance['title'] ) ? $instance['title'] : '';
    $content = isset( $instance['content'] ) ? $instance['content'] : '';

    return array(
        'title' => $title,
        'content' => $content,
    );
  }

}

siteorigin_widget_register('sow-partner', __FILE__, 'SOW_Partner');
