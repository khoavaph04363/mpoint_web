<?php

class SOW_Background extends SiteOrigin_Widget {

  function __construct() {

    parent::__construct(
        'sow-background', __('Block hiển thị ảnh nền', 'mpoint'), array(
        'description' => __('Block hiển thị ảnh nền(không chữ)', 'mpoint'),
    ), array(), false, plugin_dir_path(__FILE__)
    );
  }

  /**
   * Initialize the image grid, mainly to add scripts and styles.
   */
  function initialize() {

  }

  function get_widget_form() {

    return array(
        'background' => array(
            'type' => 'media',
            'label' => __('Ảnh nền', 'mpoint')
        ),
    );
  }

  function get_template_variables( $instance, $args ) {
    $background = isset( $instance['background'] ) ? $instance['background'] : '';

    return array(
        'background' => $background,
    );
  }

}

siteorigin_widget_register('sow-background', __FILE__, 'SOW_Background');
