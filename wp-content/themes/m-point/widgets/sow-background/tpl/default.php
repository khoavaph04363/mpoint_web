<div class="c-background" data-aos="fade-up" data-aos-duration="1500">
    <div class="container">
        <img src="<?= wp_get_attachment_image_url($background, [1920, 0]) ?>">
    </div>
</div><!-- c-background -->