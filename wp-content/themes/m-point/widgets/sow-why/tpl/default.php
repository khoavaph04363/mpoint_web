<div class="c-block" id="why" style="background-image: url('<?= wp_get_attachment_image_url($background, [1920, 0]) ?>')">
    <div class="container">
        <div class="c-box">
            <?php if (!empty($title)): ?>
                <div class="c-box__title">
                    <?= $title; ?>
                </div>
            <?php endif; ?>
            <?php if (!empty($contents)): ?>
                <div class="c-landing-grid" data-aos="fade-down" data-aos-duration="1500">
                    <ul class="clearfix owl-carousel">
                        <?php foreach ($contents as $content) : ?>
                            <li>
                                <div class="c-landing-grid__outer">
                                    <?php if (!empty($content['image'])): ?>
                                        <img alt="why"
                                             src="<?= wp_get_attachment_image_url($content['image'], [1920, 0]) ?>">
                                    <?php endif; ?>
                                    <?php if (!empty($content['content'])): ?>
                                        <span><?= $content['content']; ?></span>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div><!-- c-block -->