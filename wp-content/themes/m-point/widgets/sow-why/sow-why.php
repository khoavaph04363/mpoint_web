<?php
/*
Widget Name: Image Grid
Description: Display a grid of images. Also useful for displaying client logos.
Author: SiteOrigin
Author URI: https://siteorigin.com
*/

class SOW_Why extends SiteOrigin_Widget {

	function __construct(){

		parent::__construct(
			'sow-why',
			__('Block "Tại sao chọn chúng tôi"', 'mpoint'),
			array(
				'description' => __('Block hiển thị Nội dung "Tại sao chọn chúng tôi"', 'mpoint'),
			),
			array(),
			false,
			plugin_dir_path( __FILE__ )
		);
	}

	/**
	 * Initialize the image grid, mainly to add scripts and styles.
	 */
	function initialize(){
	}

	function get_widget_form(){

		return array(
            'background' => array(
                'type' => 'media',
                'label' => __('Ảnh nền', 'mpoint')
            ),
			'title' => array(
				'type' => 'text',
				'label' => __('Tiêu đề', 'mpoint')
			),
			'contents' => array(
				'type' => 'repeater',
				'label' => __('Nội dung', 'mpoint'),
				'item_name'  => __( 'Nội dung', 'mpoint' ),
				'item_label' => array(
					'selector'     => "[name*='title']",
					'update_event' => 'change',
					'value_method' => 'val'
				),
				'fields' => array(
					'image' => array(
						'type' => 'media',
						'label' => __('Ảnh', 'mpoint')
					),
					'content' => array(
						'type' => 'text',
						'label' => __('Nội dung', 'mpoint')
					)
				)
			),
		);
	}
	
	function get_template_variables( $instance, $args ) {
        $background = isset( $instance['background'] ) ? $instance['background'] : '';
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$contents = isset( $instance['contents'] ) ? $instance['contents'] : array();
		
		return array(
            'background' => $background,
			'title' => $title,
			'contents' => $contents,
		);
	}

}

siteorigin_widget_register( 'sow-why', __FILE__, 'SOW_Why' );
