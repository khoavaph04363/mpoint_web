<div class="c-landing-email">
    <div class="container">
        <?php if (!empty($title)): ?>
            <div class="c-landing-email__title">
                <?= $title; ?>
            </div>
        <?php endif; ?>
        <?php if (!empty($content)): ?>
            <div class="c-landing-email__des">
                <?= $content; ?>
            </div>
        <?php endif; ?>
        <?php if (!empty($contact_form)): ?>
            <div class="c-landing-email__form">
                <?php echo do_shortcode($contact_form); ?>
            </div>
        <?php endif; ?>
    </div>
</div>