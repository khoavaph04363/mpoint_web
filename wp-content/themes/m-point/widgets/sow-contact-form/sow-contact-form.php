<?php

class SOW_Contact_Form extends SiteOrigin_Widget {

  function __construct() {

    parent::__construct(
        'sow-contact-form', __('Block Điền thông tin Email', 'mpoint'), array(
        'description' => __('Block hiển thị trường điền email(Contact Form 7)', 'mpoint'),
    ), array(), false, plugin_dir_path(__FILE__)
    );
  }

  /**
   * Initialize the image grid, mainly to add scripts and styles.
   */
  function initialize() {

  }

  function get_widget_form() {

    return array(
        'title' => array(
            'type' => 'text',
            'label' => __('Tiêu đề', 'mpoint'),
        ),
        'content' => array(
            'type' => 'tinymce',
            'label' => __('Nội dung', 'mpoint'),
            'rows' => 10
        ),
        'contact_form' => array(
            'type' => 'text',
            'label' => __('Shortcode Contact Form', 'mpoint'),
        ),
    );
  }

  function get_template_variables( $instance, $args ) {
    $title = isset( $instance['title'] ) ? $instance['title'] : '';
    $content = isset( $instance['content'] ) ? $instance['content'] : '';
    $contact_form = isset( $instance['contact_form'] ) ? $instance['contact_form'] : '';

    return array(
        'title' => $title,
        'content' => $content,
        'contact_form' => $contact_form,
    );
  }

}

siteorigin_widget_register('sow-contact-form', __FILE__, 'SOW_Contact_Form');
