<?php get_header(); ?>
<?php
$bannerimgdefault = ot_get_option('banner_bg');
?>
<div class="l-banner__detail" style="background-image: url('<?php echo $bannerimgdefault; ?>');">
	<div class="c-caption">
		<div class="container">
			<div class="b-table">
				<div class="b-table__row">
					<div class="b-table__title">
						<?php single_cat_title(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="c-block-2 is-breadcrumb">
	<div class="container">
		<div class="c-yoast-breadcrumb">
			<?php if (function_exists('yoast_breadcrumb')) {
				yoast_breadcrumb();
			} ?>
		</div>
	</div>
</div>
<div class="c-list-category">
	<div class="container">
		<ul class="clearfix">
			<?php wp_list_categories('title_li=&order=DESC&hide_empty=0'); ?>
		</ul>
	</div><!-- container -->
</div><!-- c-breadcrumb-outer -->
<div class="l-content">
	<div class="container">
		<div class="c-news-grid js-match-height">
			<ul class="clearfix">
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
					<li id="post-<?php the_ID(); ?>">
						<?php get_template_part('content-news'); ?>
					</li>
				<?php endwhile; ?>
				<?php else: ?>
					<li class="nodata" style="width: 100%;">Hiện tại không có bài viết nào!</li>
				<?php endif; ?>
			</ul>
		</div><!-- c-news-grid -->
		<?php get_template_part('pagination'); ?>
	</div><!-- container -->
</div><!-- l-content -->
<?php get_footer(); ?>