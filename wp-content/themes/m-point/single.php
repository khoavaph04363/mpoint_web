<?php get_header(); ?>
<?php
$bannerimgdefault = ot_get_option('banner_bg');
$bannerimg = has_post_thumbnail();
$size = 'full';
$bannerimgurl = wp_get_attachment_image_src($bannerimg, $size);
?>
<?php if($bannerimg){ ?>
	<div class="l-banner__detail" style="background-image: url('<?php echo the_post_thumbnail_url(); ?>');">
	<?php } else { ?>
		<div class="l-banner__detail" style="background-image: url('<?php echo $bannerimgdefault; ?>');">
		<?php } ?>
		<div class="c-caption">
			<div class="container">
				<div class="b-table">
					<div class="b-table__row">
						<div class="b-table__title">
							<?php the_title(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="c-block-2 is-breadcrumb">
		<div class="container">
			<div class="c-yoast-breadcrumb">
				<?php if (function_exists('yoast_breadcrumb')) {
					yoast_breadcrumb();
				} ?>
			</div>
		</div>
	</div>
	<div class="l-content">
		<div class="container">
			<div class="c-news-detail">
				<div class="c-news-detail__title"><h1><?php the_title(); ?></h1></div>
				<div class="c-news-detail__time"><i class="fa fa-clock-o" aria-hidden="true"></i><span><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' trước'; ?></span>
				</div>
				<div class="b-maincontent">
					<?php the_content(); ?>
				</div><!-- b-maincontent -->
				<?php if ( has_tag()) : ?>
					<div class="c-tags">
						<label><i class="fa fa-tags"></i>Tags:</label>
						<div class="c-tags__content">
							<?php the_tags(' ', ' '); ?>
						</div>
					</div><!-- c-tags -->
				<?php endif; ?>
			</div><!-- c-news-detail -->
		</div><!-- container -->
	</div><!-- l-content -->

	<?php get_footer(); ?>