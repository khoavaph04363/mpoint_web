<?php

include_once('includes/function_register_menus.php');
include_once('includes/function_custom_nav_walker.php');
include_once('includes/function_pagination.php');
include_once('includes/function_custom_excerpt.php');
include_once('includes/function_custom_hook.php');

include_once('widgets/sow-banner/sow-banner.php');
include_once('widgets/sow-why/sow-why.php');
include_once('widgets/sow-landing-grid/sow-landing-grid.php');
include_once('widgets/sow-background/sow-background.php');
include_once('widgets/sow-smart-feature/sow-smart-feature.php');
include_once('widgets/sow-background-popup/sow-background-popup.php');
include_once('widgets/sow-download/sow-download.php');
include_once('widgets/sow-title-img/sow-title-img.php');
include_once('widgets/sow-contact-form/sow-contact-form.php');
include_once('widgets/sow-partner/sow-partner.php');

if (function_exists('add_theme_support')) {
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
		add_post_type_support( 'page', 'excerpt' );
    add_image_size('1920', 1920, 0, true);
}

function scripts()
{

    $staticVer = exec('git rev-parse HEAD');
    //Lib style
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/static/css/bootstrap.min.css', [], $staticVer);
    wp_enqueue_style('fontawe', get_template_directory_uri() . '/static/css/fontawesome-all.min.css', [], $staticVer);
    wp_enqueue_style('aos', get_template_directory_uri() . '/static/css/aos.css', [], $staticVer);
    wp_enqueue_style('carousel', get_template_directory_uri() . '/static/css/owl.carousel.min.css', [], $staticVer);
    wp_enqueue_style('animate', get_template_directory_uri() . '/static/css/animate.css', [], $staticVer);
    wp_enqueue_style('theme-style', get_template_directory_uri() . '/static/css/style.min.css', [], $staticVer . ot_get_option('file_version', '1.0'));

    // Load our main stylesheet.
    wp_enqueue_style('style', get_stylesheet_uri(), ['bootstrap']);

    //Js
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri() . '/static/js/jquery-3.3.1.min.js', [], $staticVer, true);
    wp_enqueue_script('migrate', get_template_directory_uri() . '/static/js/jquery-migrate-1.4.1.js', ['jquery'], $staticVer, true);
    wp_enqueue_script('popper', get_template_directory_uri() . '/static/js/popper.min.js', ['jquery'], $staticVer, true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/static/js/bootstrap.min.js', ['jquery'], $staticVer, true);
    wp_enqueue_script('carousel', get_template_directory_uri() . '/static/js/owl.carousel.min.js', ['jquery'], $staticVer, true);
    wp_enqueue_script('match', get_template_directory_uri() . '/static/js/jquery.matchHeight-min.js', ['jquery'], $staticVer, true);
    wp_enqueue_script('aos', get_template_directory_uri() . '/static/js/aos.js', ['jquery'], $staticVer, true);
    wp_enqueue_script('function', get_template_directory_uri() . '/static/js/function.js', [], $staticVer . ot_get_option('file_version', '1.0'), true);
    wp_enqueue_script('style', get_template_directory_uri() . '/static/js/style.js', [], $staticVer . ot_get_option('file_version', '1.0'), true);
    
}

add_action('wp_enqueue_scripts', 'scripts');