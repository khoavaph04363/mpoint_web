<?php get_header(); ?>

  <div class="c-breadcrumb-outer">
    <div class="container">
      <div class="c-yoast-breadcrumb">
        <?php if (function_exists('yoast_breadcrumb')) {
          yoast_breadcrumb();
        } ?>
      </div><!-- c-yoast-breadcrumb -->
    </div><!-- container -->
  </div><!-- c-breadcrumb-outer -->
  <div class="l-content">
    <div class="container">
      <div class="c-news-detail">
        <div class="c-news-detail__title"><h1>Trang hiện tại đang bị lỗi</h1></div>
      </div><!-- c-news-detail -->
    </div><!-- container -->
  </div><!-- l-content -->

<?php get_footer(); ?>